/*
 * l3gd20.c
 *
 *  Created on: 18.11.2014
 *      Author: chris
 */

#include "l3gd20.h"
#include "spi.h"
#include <FreeRTOS.h>
#include "printf.h"

static void* spi_mems = 0;

int l3gd20_init(void) {

	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	spi_mems= spi_open(2);
	uint8_t cmd = (0x00|(1<<7 /*read bit*/))|0x0F;
	uint8_t resp = 0x00;
	spi_write_partial(spi_mems, (void*)&cmd, 1,xDelay);
	spi_read(spi_mems,(void*)&resp,1,xDelay);

	printf("Who am I: 0x%X\n",resp);
	return 1;
}
